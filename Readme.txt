﻿Numbering 

Program pro vypocet znamky studentu z dilcich znamek



Autor: Pavel David

Files: numbering.cpp - source file
       numbering.exe - executable
       Readme.txt - information about project

Pozn. Na Githubu se obvykle pouziva .md file pro
popis dokumentace projektu, ja jsem pouzil jen simple
txt file pro jednoduchost.

===============
Version history
===============

version 1.0
- starting version
Jednoduchy koncept.
Program pocital znamku studenta jako soucet
znamky ze zkousky v polovine, na konci a z prumerne
hodnoty znamek z domacich ukolu.

version 1.1.0
- zmena verzovaciho systemu 
pouzivame tri cisla verze X.Y.Z
X - pro major change
Y - pro minor change
Z - hot fix
- zmena vypoctu znamky z domacich ukolu z prumerne na stredni
hodnotu znamek z domacich ukolu (hodnoty znamek z domacich ukolu
se ukladaji do vektoru)
- pridali jsme Readme.txt popisujici zmeny v projektu


version 1.2.0

zavadime novou datovou strukturu - Student_info
obsahujici string jmeno
polozky    double polovina, zaver (reprezentujici znamky studenta ze zkousek)
a vector double domaciukol

pomocne funkce rozdelime do 3 zdrojovych souboru a jim odpovidajich hlavickovych
souboru (obsahujicich deklarace funkci)

znamka.cpp - obsahuje definice funkci:
           double znamka(double, double, double) ... pouziva funkci stred
           double znamka(double, double, const std::vector<double>&)  ... vola predchozi funkci znamka
           double znamka(const Student_info&) ... vola predchozi funkci znamka 
znamka.h - obsahuje deklarace výše zmíněných funkcí

stred.cpp - obsahuje definici funkce 
         double stred(vector<double> vec)    - ktera pocita stredni hodnotu hodnot
                                               ve vektoru hodnot double 
stred.h

Student_info.cpp - definice structury Student_info

  funkci: 
           bool    porovnani(const Student_info&, const Student_info&)
           std::istream& cteni(std::istream&, Student_info&)
           std::istream& cteni_du(std::istream&, std::vector<double>&);

pracujicimi s objekty typu Student_info
Student_info.h

numbering.cpp - telo hlavniho programu

input - cteni parametru objektu Student_info
serazeni objektu Student_info, abecedne podle jmena
vypocet zaverecne znamky (souslednost volani funkci):
znamka(Student_info&) -> znamka(std::vector<double>&> -> znamka(double) -> stred(vector<double>)



Hlavni zmeny
1. Vyuzivame nekolik zdrojovych souboru a jim odpovidajich hlavickouvych souboru
2. Pro spravne slinkovani zdrojovych souboru jsme vytvorili projekt v Dev C++
   numbering.dev
3. Program umi nove nacitat zaznamy o vice studentech do vektoru typu Student_info
4. Vypisuje jmena studentu a vyslednou znamku, vystup je vypsan tak aby zbyvajici 
jmena studentu kratsi nez nejdelsi jmeno studenta byla doplnena mezerami na jeho delku

Zname problemy: znamky z domacich ukolu je potreba ukoncit znakem konce souboru
Ctrl + Z. Jinak dojde k chybnemu nacitani v dalsim cyklu promene 

pr. zadame znamky z domacich ukolu jako: 2 3 4 2 1 2 f
pri znaku f dojde k selhani cteni (ale znak f je uz nacten)
pri dalsim volani cyklu se automaticky f pouzije jako jmeno dalsiho studenta
a program skoci rovnou na radek nacitani znamek z poloviny a na konci kurzu

