#ifndef HLIDAC_Student_info_h
#define HLIDAC_Student_info_h

//hlavickovy soubor Student_info.h
#include <iostream>
#include <string>
#include <vector>

struct Student_info {
	std::string jmeno;
	double polovina, zaver;
	std::vector<double> domaciukol;
};

bool porovnani(const Student_info&, const Student_info&);

std::istream& cteni(std::istream&, Student_info&);

std::istream& cteni_du(std::istream&, std::vector<double>&);

#endif
