#ifndef HLIDAC_znamka_h
#define HLIDAC_znamka_h

// hlavickovy soubor znamka.h
#include <vector>
#include "Student_info.h"

double znamka(double, double, double);
double znamka(double, double, const std::vector<double>&);
double znamka(const Student_info&);

#endif

