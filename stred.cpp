//zdrojovy soubor funkce stred

#include <algorithm>  // pouzivame funkci sort
#include <stdexcept>  // pouzivame domain_error
#include <vector>     // pouzivame vector

using std::domain_error;    using std::sort;
using std::vector;

//spocita stredni hodnotu z vector<double>
double stred(vector<double> vec)
{
	typedef vector<double>::size_type vec_vel;
	
	vec_vel velikost = vec.size();
	
	if (velikost == 0)
	    throw domain_error("Stredni hodnota prazdneho  vektoru");
	    
	sort(vec.begin(), vec.end());
	
	vec_vel pros = velikost/2;
	
	return velikost % 2 == 0 ? (vec[pros] + vec[pros-1]) / 2 : vec[pros];
}
