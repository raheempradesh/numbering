//zdrojovy soubor pro znamka.h

#include <vector>
#include <stdexcept>
#include "znamka.h"
#include "stred.h"
#include "Student_info.h"

using std::vector;    using std::domain_error;

// spocita zaverecnou znamku studenta ze znamek zkousky v polovine, 
// na zaver kurzu a z domacich ukolu  
double znamka(double polovina, double zaver, double domaciukol)
{
	return 0.2 * polovina + 0.4 * zaver + 0.4 * domaciukol;
}

// spocita zaverecnou znamku ze znamek zkousky v polovine, na zaver kurzu
// a vectoru znamek z domacich ukolu. Tato funkce nekopiruje svuj argument protoze to 
// pro nas dela funkce stred
double znamka(double polovina, double zaver, const vector<double>& du)
{
	if (du.size() == 0)
	    throw domain_error("student neudal zadny domaci ukol");
	
	return znamka(polovina, zaver, stred(du));
}

// pocita zaverecnou  znamku struktury Student_info odkazuje se
// na funkci znamka(double, double, const vector<double>&)
 double znamka(const Student_info& s)
 {
 	return znamka(s.polovina, s.zaver, s.domaciukol);
 }
