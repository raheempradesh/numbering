#include <iostream>
#include <iomanip>
#include <ios>
#include <string>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include "znamka.h"
#include "Student_info.h"

using std::cin;          using std::string;
using std::cout;         using std::streamsize;
using std::endl;         using std::sort;
using std::setprecision; using std::vector;
using std::domain_error; using std::istream;
using std::max;

int main()
{
	vector<Student_info> studenti;
	Student_info zaznam;
	string::size_type maxdelka = 0;    // delka nejdelsiho jmena
	
	// nacte a ulozi vsechna data studentu
	// Invariant: studenti obsahuje vsechny doposud nactene zaznamy studentu
	// maxdelka obsahuje delku nejdelsiho jmena v studenti  
	while (cteni(cin, zaznam)) {
		// nalezne delku nejdelsiho jmena
		maxdelka = max(maxdelka, zaznam.jmeno.size());
		studenti.push_back(zaznam);
	}
	
	// seradi zaznamy podle abecedy
	sort(studenti.begin(), studenti.end(), porovnani);
	
	//oddeli vstup od vystupu
	cout << endl << "Vysledne znamky studentu: " << endl;
	
	// vypise jmena a znamky
	for (vector<Student_info>::size_type i=0; i != studenti.size(); ++i) {
		
		// vypise jmeno doplnene zprava na maxdelka + 1 znaku
		cout << studenti[i].jmeno << string(maxdelka + 1 - studenti[i].jmeno.size(), ' ');
		
		// spocita a vypise znamku
		try {
			double zaverecna_znamka = znamka(studenti[i]);
			streamsize presnost = cout.precision();
			cout << "zaverecna znamka je " << setprecision(3)
	         << zaverecna_znamka << setprecision(presnost);
		} catch (domain_error e) {
			cout << e.what();
		}
		cout << endl;
	}
	return 0;
}

