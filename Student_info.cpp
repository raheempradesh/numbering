//zdrojovy soubor funkci vztazenych k Student_info

#include "Student_info.h"

using std::istream;    using std::vector;    using std::cout;
using std::endl;

bool porovnani(const Student_info& x, const Student_info& y)
{
	return x.jmeno < y.jmeno;
}

istream& cteni(istream& vstup, Student_info& s)
{
	// nacte a ulozi studentovo jmeno a znamky v polovine a na zaver kurzu
	cout << "Zadejte studentovo jmeno: ";
	vstup >> s.jmeno;
	
	cout << endl << "Zadejte znamky v polovine a na zaver kurzu: "; 
	vstup >> s.polovina >> s.zaver;
	
	cteni_du(vstup, s.domaciukol);    // nacte a ulozi znamky z domacich ukolu
	return vstup;
}

istream& cteni_du(istream& vstup, vector<double>& du)
{
	if (vstup) {
		// vymaze predchozi obsah vektoru du
		du.clear();
		
		// nacte znamky studenta z domacich ukolu
		double x;
		cout << "Zadejte znamky domacich ukolu nasledovane koncem souboru: " << endl;
		while (vstup >> x)
		du.push_back(x);
		
		// vycisti proud, aby vstup fungoval spravne i pro dalsiho studenta
		vstup.clear();
	}
	return vstup;
}
